from django.db import models
# cari tahu gimana masukkin waktu ke modelsnya
# Create your models here.
class Mod(models.Model):

    kegiatan = models.CharField(max_length=120)
    kategori = models.CharField(max_length=120, default='immediate')
    tempat = models.CharField(max_length=120, default = 'Depok')
    tanggal_kegiatan = models.DateField("Event Date")
    waktu_kegiatan = models.TimeField("Waktu kegiatan")

    def __str__(self):
        return "{}.{}".format(self.id, self.nama_kegiatan)