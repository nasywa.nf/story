from django.forms import ModelForm
from .models import Mod

class ModForms(ModelForm):
    class Meta:
        model = Mod
        
        fields = [
            'kegiatan',
            'tanggal_kegiatan',
            'waktu_kegiatan',
            'tempat',
            'kategori',
        ]