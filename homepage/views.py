from django.shortcuts import render, get_object_or_404, redirect
from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import ModForms
from .models import Mod
from datetime import datetime

def portofolio(request):
    return render(request, 'portofolio.html')
def profile(request):
    return render(request, 'profile.html')
def forms_view(request):
    response = {}
    form = ModForms(request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
        response['kegiatan'] = request.POST['kegiatan']
        response['tanggal_kegiatan'] = request.POST['tanggal_kegiatan']
        response['waktu_kegiatan'] = request.POST['waktu_kegiatan']
        response['kategori'] = request.POST['kategori']
        response['tempat'] = request.POST['tempat']

        mod = Mod(kegiatan=response['kegiatan'], tanggal_kegiatan=response['tanggal_kegiatan'], waktu_kegiatan=response['waktu_kegiatan'], kategori=response['kategori'], tempat=response['tempat'])
        mod.save()
        form = ModForms(request.POST or None)

    context = {
        'form':form,
        'all_mod':Mod.objects.all(),
    }

    return render(request,"forms.html",context)

def view_form(request):
    data = Mod.objects.all()
    return render(request, 'forms.html', {'data': data})

def delete_activity(request, pk):
    activity = get_object_or_404(Mod, pk=pk)
    activity.delete()

    return redirect('/jadwal')

# Create your views here.
