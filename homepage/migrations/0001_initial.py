# Generated by Django 2.2.6 on 2019-10-07 09:49

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Mod',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('kegiatan', models.CharField(max_length=120)),
                ('tanggal_kegiatan', models.DateField(verbose_name='Event Date')),
                ('waktu_kegiatan', models.TimeField(verbose_name='Waktu kegiatan')),
            ],
        ),
    ]
